# ModuleSettingChanged

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**module_guid** | **str** |  | [optional] [readonly] 
**hub_guid** | **str** |  | [optional] [readonly] 
**key** | **str** |  | [optional] [readonly] 
**value** | **str** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


