# sencity_client.ModulesApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modules_settings_changed_read**](ModulesApi.md#modules_settings_changed_read) | **GET** /modules/settings/changed/{unix_timestamp} | 


# **modules_settings_changed_read**
> list[ModuleSettingChanged] modules_settings_changed_read(unix_timestamp)



### Example

* Basic Authentication (Basic):
```python
from __future__ import print_function
import time
import sencity_client
from sencity_client.rest import ApiException
from pprint import pprint
configuration = sencity_client.Configuration()
# Configure HTTP basic authorization: Basic
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://localhost:8000
configuration.host = "http://localhost:8000"
# Enter a context with an instance of the API client
with sencity_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sencity_client.ModulesApi(api_client)
    unix_timestamp = 'unix_timestamp_example' # str | 

    try:
        api_response = api_instance.modules_settings_changed_read(unix_timestamp)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ModulesApi->modules_settings_changed_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unix_timestamp** | **str**|  | 

### Return type

[**list[ModuleSettingChanged]**](ModuleSettingChanged.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

