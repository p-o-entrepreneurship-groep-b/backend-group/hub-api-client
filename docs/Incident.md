# Incident

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hub_guid** | **str** |  | 
**timestamp** | **datetime** |  | 
**module_guid** | **str** |  | 
**payload** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


