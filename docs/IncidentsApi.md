# sencity_client.IncidentsApi

All URIs are relative to *http://localhost:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**incidents_create**](IncidentsApi.md#incidents_create) | **POST** /incidents/ | 


# **incidents_create**
> Incident incidents_create(data)



### Example

* Basic Authentication (Basic):
```python
from __future__ import print_function
import time
import sencity_client
from sencity_client.rest import ApiException
from pprint import pprint
configuration = sencity_client.Configuration()
# Configure HTTP basic authorization: Basic
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://localhost:8000
configuration.host = "http://localhost:8000"
# Enter a context with an instance of the API client
with sencity_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sencity_client.IncidentsApi(api_client)
    data = sencity_client.Incident() # Incident | 

    try:
        api_response = api_instance.incidents_create(data)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling IncidentsApi->incidents_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Incident**](Incident.md)|  | 

### Return type

[**Incident**](Incident.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

