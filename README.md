# sencity-client
No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: v1
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.PythonClientCodegen

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import sencity_client
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import sencity_client
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function
import time
import sencity_client
from sencity_client.rest import ApiException
from pprint import pprint

configuration = sencity_client.Configuration()
# Configure HTTP basic authorization: Basic
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://localhost:8000
configuration.host = "http://localhost:8000"
# Enter a context with an instance of the API client
with sencity_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sencity_client.IncidentsApi(api_client)
    data = sencity_client.Incident() # Incident | 

    try:
        api_response = api_instance.incidents_create(data)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling IncidentsApi->incidents_create: %s\n" % e)
    
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8000*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*IncidentsApi* | [**incidents_create**](docs/IncidentsApi.md#incidents_create) | **POST** /incidents/ | 
*ModulesApi* | [**modules_settings_changed_read**](docs/ModulesApi.md#modules_settings_changed_read) | **GET** /modules/settings/changed/{unix_timestamp} | 


## Documentation For Models

 - [Incident](docs/Incident.md)
 - [ModuleSettingChanged](docs/ModuleSettingChanged.md)


## Documentation For Authorization


## Basic

- **Type**: HTTP basic authentication


## Author




